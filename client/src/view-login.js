import { LitElement, html, css } from 'lit-element';

class ViewLogin extends LitElement {

    /**
     * Sets the css styling for render().
     */
    static get styles() {
        return css `

            body {font-family: Arial, Helvetica, sans-serif;}
            form {border: 3px solid #f1f1f1;}

            input[type=email], input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            box-sizing: border-box;
            }

            button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
            }

            button:hover {
            opacity: 0.8;
            }

            .cancelbtn {
            width: auto;
            padding: 10px 18px;
            background-color: #f44336;
            }

            .imgcontainer {
            text-align: center;
            margin: 24px 0 12px 0;
            }

            img.avatar {
            width: 40%;
            border-radius: 50%;
            }

            .container {
            padding: 16px;
            }

            span.psw {
            float: right;
            padding-top: 16px;
            }

            /* Change styles for span and cancel button on extra small screens */
            @media screen and (max-width: 300px) {
            span.psw {
                display: block;
                float: none;
            }
            .cancelbtn {
                width: 100%;
            }
            }
        `;
    }

    /**
     * Sets enviroment type aliases.
     * 
     * @returns key-value pairs.
     */
    static get properties() {
        return {
            email: String,
            password: String,
        };
    }

    constructor() {
        super();

        this.email = "";
        this.password = "";
    }

    /**
     * Renders the login page.
     */
    render() {
        return html `
        <h2>Login Form</h2>

        <p>Fill in the form to sign in!</p>
        
        <form action=" " method="post">
          <div class="container">
            <label for="email"><b>Email</b></label>
            <input type="email" placeholder="Enter Email" name="email" @input=${(e) => this._onInputEmail(e)} id= inputEmail required pattern="">
        
            <label for="psw"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" @input=${(e) => this._onInputPassword(e)} id= inputPassword required pattern="">
                
            <button type="click" @click=${this.login}>Login</button>
            <label>
              <input type="checkbox" checked="checked" name="remember"> Remember me
            </label>
          </div>
        
          <div class="container" style="background-color:#f1f1f1">
            <a href="/" class="cancelbtn">Cancel</a>
            <span class="psw">Don't have an account? <a href="/register">Register.</a></span>
          </div>

        </form>

        </body>
    `;
    }


    /**
     * This function sends login information to the server
     * to check existing data inside the database
     * 
     * @param e - data from the login form 
     */
    login(e) {

        var data = {
            "email": this.email,
            "password": this.password
        }
        e.preventDefault();
        fetch(`${window.MyAppGlobals.serverURL}login`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            })
            .then(res => res.json())
            .then(json => console.log(json))
    }

    /**
     * @param e - data from the login form
     */
    _onInputEmail(e) {
        this.email = e.currentTarget.value;
    }

    /**
     * @param e - data from the login form.
     */
    _onInputPassword(e) {
        this.password = e.currentTarget.value;
    }


}
customElements.define('view-login', ViewLogin);