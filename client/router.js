import {Router} from '@vaadin/router';
import '/src/landing-page.js'
import '/src/view-login.js'
import '/src/view-register.js'


const landing = document.getElementById('landing');
const router = new Router (landing);

router.setRoutes([{path: '/',         component: 'landing-page'},
                  {path: '/login',    component: 'view-login'     },
                  {path: '/register', component: 'view-register'  },
                ]);